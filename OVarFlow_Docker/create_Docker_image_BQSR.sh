#!/usr/bin/env bash

cp ../OVarFlow_src/Snakefile ./
cp ../OVarFlow_BQSR/SnakefileBQSR ./
cp ../OVarFlow_BQSR/BQSR_dependencies_mini.yml ./

# the Docker image should contain some additional applications
echo "  - time=1.8" >> BQSR_dependencies_mini.yml

cp -r ../OVarFlow_src/scripts ./

docker build -t ovarflow/test_bqsr:`date +%b%d_%G` -f Dockerfile_BQSR .
