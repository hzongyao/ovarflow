Creation of a Docker Image for OVarFlow
==========================================

Two conditions must be fulfilled to create the Docker container:
1. a working Docker installation
2. a local copy of the OpenVaraint repository (or at least the directories ``OVarFlow_Docker`` and OVarFlow_src``)

Now the OVarFlow Docker image can easily be created by invoking the shell script:
```
./create_Docker_image.sh
```
This script first copies all nessessary files from the ``OVarFlow_src`` directory and then builds the Docker image.


Singularity conversion
----------------------

The local Docker image (see ``docker images``) can also easily be converted into a Singularity container:
```
singularity build [your_image_name].sif docker-daemon://[REPOSITORY]:[TAG]
```


Container creation for the BQSR workflow
----------------------------------------

The BQSR workflow is more complex and requires some additional software. Therefore a second container can be created, that contains both workflows, the normal workflow (like the above Docker image) and the BQSR workflow. Therefore those user, that only want to use the normal workflow, can be provided with a smaller container.

The procedure to create the Docker image is basically identical to the above procedure. Three directories have to be copied from the repository (*OVarFlow_BQSR, OVarFlow_Docker, OVarFlow_src*) and a script has to be invoked inside the Docker directory:
```
./create_Docker_image_BQSR.sh
```

Of course a locally available BQSR Docker image can be converted into a Singularity container as well:
```
singularity build [your_image_name].sif docker-daemon://[REPOSITORY]:[TAG]
```
