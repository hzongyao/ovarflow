Benchmarking of Java Garbage Collection
=======================================

This collection of scripts is meant to measure the influence of Java GC on the resource usage of:
* CPU load wall time
* CPU load user time
* resident set size (RSS)
Values are measured via GNU time (`/usr/bin/time`) version 1.8. Version 1.7 exhibits a bug that will give four times to high values for RSS.

Before the actual benchmarking the OVarFlow workflow has to be executed with the files that shall be used for benchmarking. The files that are generated within this data evaluation can then be used for the actual benchmarking.

Measurement and evaluation of results is done in three steps:
1. perform the measurement:
   ```
   ./benchmarking_GATK_GC <tool_name> <SRR_file> [Xmx_value]
   ```
   `tool name` can be: `SortSam`, `MarkDuplicates`, `HaplotypeCaller`, `CombineGVCFs` or `GatherVcfs`<br>
   Results of the benchmarking, meaning GNU time measurements, will be stored in the directory `bm_Java_GC`.<br>
   An example invocation looks like this:
   ```
   ./benchmarking_GATK_GC.sh SortSam SRR3041137 12
   ```
2. convert the format of the results:
   ```
   ./convert_format.sh <GNU_time_results> > <output_values>
   ```
3. create an image:
   ```
   ./multiplot.R <previous_output_values> <tool_name>
   ```

Alternatively benchmarking of all the above mentionend GATK applications can be started by:
```
./bm_GC_all.sh
```
