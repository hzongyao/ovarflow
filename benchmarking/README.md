Benchmarking of OVarFlow
===========================

Benchmarking of OVarFlow was performed in two different respects:

* Influence of the number of Java Garbage Collection (GC) threads<br>
  Here GNU time (`/usr/bin/time`) was used to measure three values:

  * wall time
  * user time
  * maximum resident set size (RSS)<br><br>

  The scripts that were used to obtain the measurements and final graphics can be found in the directory `GNU_GC_measurement`.

* Continuous monitoring of CPU and memory usage<br>
  Within those measurements the influence of sequencing depth or file size was monitored, respectively. Also a profile of the continuous resource usage was obtained. Monitoring of the resource usage was achieved by a shell script (the respective command that shall be monitored has to be modified within this script) and plots were generate using R scripts. Those scripts are found within the directory `continuous_CPU_RAM_load`.
