#!/usr/bin/env bash

# usage:
# ./format_time_sar.sh <input file>

# Eingabedatei
FILE=$1

# die Kopfzeile zu den Spalten ausgeben
# 13 Spalten bei mpstat
# 17 Spalten bei sar, kbslab Spalte 14
# zuviele Spalten stören awk aber nicht
head -3 $FILE | tail -1 | awk '{print "seconds", $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15}'

# immer bei einem Zeitsprung AM -> PM -> AM ... müssen
# 12 Stunden hinzuaddiert werden
tail -n +4 $FILE | awk 'BEGIN{stunden=0}
{
	# erstmal einen Startwert für die Zeit bestimmen
	if (NR == 1) {
		last = zuSekunden($1)
		base = zuSekunden($1) # den Ausgangswert der Sekunden bestimmen
		# 13te Spalte nur bei mpstat, sar nur 12 Zeilen
		print last-base, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15
	} else {
		aktuell = zuSekunden($1)
		# falls nun weniger Sekunden wird zuvor 12 h hinzuzählen
		if ( aktuell < last ) {
			stunden += 12 * 60 * 60
			zeit = aktuell + stunden
		}
		# 13te Spalte nur bei mpstat, sar nur 12 Zeilen
		print aktuell-base + stunden, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15

		last = aktuell
	}
}

function zuSekunden(zeit)
{
	split(zeit, a, ":")
	sekunden = a[1]*60*60 + a[2]*60 + a[3]
	return sekunden
}'
