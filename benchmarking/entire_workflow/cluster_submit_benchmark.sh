#!/bin/bash 

#$ -q all.q 
#$ -cwd 
#$ -V
#$ -S /bin/bash
#$ -o output.txt 
#$ -e error.txt 
#$ -pe multislot 40 
#$ -l virtual_free=6000M 
#$ -l hostname=hp-sl230*

# Benchmarking aktivieren
# Name der ausführenden Maschine
hostname > TIME_STAMP

# Startzeit aufzeichnen:
echo "Startzeitpunkt" >> TIME_STAMP
date >> TIME_STAMP 
echo -n "Unix time: " >> TIME_STAMP
date +%s >> TIME_STAMP

# CPU-Last aufzeichnen - alle x sec
mpstat 30 > mpstat_statistics &
procIDmpstat=$!
# RAM-Nutzung aufzeichnen - alle x sec
sar -r ALL 30 > sar_statistics &
procIDsar=$!

# 30 Sek warten, da mpstat & sar erst nach 30 Sek aufzeichnen
sleep 30

# die conada environment aktivieren
. activate /vol/agluehken/CondaEnvs/BM_10_Dec20

snakemake -p -j 38 --resources mem_gb=240

# das Monitoren beenden
kill -15 $procIDmpstat
kill -9 $procIDmpstat
kill -15 $procIDsar
kill -9 $procIDsar

# Endzeitaufzeichnen
echo "Endzeitpunkt" >> TIME_STAMP
date >> TIME_STAMP
echo -n "Unix time: " >> TIME_STAMP
date +%s >> TIME_STAMP
