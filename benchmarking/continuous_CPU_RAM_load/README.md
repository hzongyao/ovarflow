Continuous CPU and RAM monitoring
=================================

Those scripts serve the monitoring and plotting of CPU and memory usage (resident set size is recorded) of a certain GATK process. Measurements are recorded every three seconds (default; can be changed). Output is written to a monitoring directory. Measurements are taken via `ps -p ${process_pid} -o rsa,%mem,%cpu`.

Measurement and evaluation of results is done in three steps:
1. perform the measurement:
   ```
   ./monitoring_script.sh > <GATK tool> <Java GC threads> <Java heap size> <input file suffix> [<seconds>]
   ```
   The `automate_script.sh` script can be used to process several files one after the other. Four different GATK tools can be selected, being SortSam, MarkDuplicates, HaplotypeCaller and CombineGVCFs.
2. copy the log (`resource_monitoring_...log`) file from the newly created monitoring directory to a working directory
3. create an image:
   Plot a single measurement:
   ```
   ./1x1_plot_CPU_RAM_usage.R <log file> <image title> <tool name>
   ```
   Plot four measurements at once, with two being repetitions:
   ```
   ./1x2_generic.R <tool name> <id data 1> <coverage data 1> <id data 2> <coverage data 2>
   ```
   Four input files have to be provided, with names of a specific format, e.g.:
   * resource\_monitoring\_137\_GC2\_Xmx2.log
   * resource\_monitoring\_137\_GC2\_Xmx2\_2.log
   * resource\_monitoring\_413\_GC2\_Xmx2.log
   * resource\_monitoring\_413\_GC2\_Xmx2\_2.log
   Which would result in the following command to plot the graphics for e.g. SortSam:
   ```
   ./1x2_generic.R SortSam 137 34 413 16
   ```
