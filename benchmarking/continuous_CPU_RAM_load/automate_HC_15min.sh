#!/usr/bin/env bash

for i in 2 4 8 12 16 20
do
	echo -e "Starting HaplotypeCaller with:\n\t$i threads\n"
	# old script see: git show 4ed43280c:./continuous_CPU_RAM_load//monitoring_script.sh
	# monitor HC with the options: threads, File, interval (1 sec)
	./monitoring_HaplotypeCaller.sh $i 137 1 &
	# evlt. mit neuem Script nötig:
	#./monitoring_script.sh HaplotypeCaller $i 4 137 &

	lastpid=$!
	echo $lastpid

	# Zeitraum der Beobachtung eines jeden Prozesses
	# 900 sec = 15 min + 1 extra
	sleep 901
	kill -15 $lastpid
	pkill java # HC läuft noch

	echo -e "Ending HaplotypeCaller with:\n\t$i threads\n"
done
