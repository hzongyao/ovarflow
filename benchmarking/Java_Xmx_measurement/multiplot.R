#!/usr/bin/env Rscript

# creates three graphs for
# wall time; user time; resident set size
#
# usage:
# ./multiplot.R <input data> <tool name> <sample name>

args = commandArgs(trailingOnly=TRUE)
FILE = args[1]
TOOL = args[2]
SAMPLE = args[3]

values = read.table(file=FILE, header=TRUE)

#**************************
# Zeitein im min umwandeln
values$user = values$user / 60
values$sys = values$sys / 60
values$wall = values$wall / 60

#***********
# wall time
# Mittelwerte berechnen
mean.wall = by(values$wall, values$Xmx, mean)
# damit eine Liste erstellen
mean.wall = list(wall=as.numeric(mean.wall), Xmx=unique(values$Xmx))

#***********
# user time
# Mittelwerte berechnen
mean.user = by(values$user, values$Xmx, mean)
# damit eine Liste erstellen
mean.user = list(user=as.numeric(mean.user), Xmx=unique(values$Xmx))

#*****
# RSS
# Werte in Gb umwandeln
values$RSS = values$RSS / 1024 / 1024
# Mittelwerte berechnen
mean.RSS = by(values$RSS, values$Xmx, mean)
# damit eine Liste erstellen
mean.RSS = list(RSS=as.numeric(mean.RSS), Xmx=unique(values$Xmx))

#*****
# Java Runtime.totalMemory() 
# Werte in Gb umwandeln
values$R.tMem = values$R.tMem / 1024 / 1024 / 1024
# Mittelwerte berechnen
mean.R.tMem = by(values$R.tMem, values$Xmx, mean)
# damit eine Liste erstellen
mean.R.tMem = list(R.tMem=as.numeric(mean.R.tMem), Xmx=unique(values$Xmx))

#**********************
# Funktion zum Plotten
#**********************

Diagramm = function(x, y,
		    y.values,
		    text.abszisse,
		    text.ordinate,
		    titel){
    # Streudiagram
    plot(x=x, y=y,
         type='l',
         ylim=c(min(y.values), max(y.values)),
         xlab=NA,
         ylab=NA,
         xaxt="n" # um die Tickmarks manuell zu bestimmen
        )
    # Achse zeichnen
    axis(side=1, at=c(4,6,8,12,16,24,48))
    # Beschriftung der Achse muss extra gezeichnet werden um die Position anzugeben
    mtext(text=text.abszisse, side=1, line=2.4, cex=0.8)
    mtext(text=text.ordinate, side=2, line=2.4, cex=0.8)
    # Streudiagramm aller Messwerte darüberlegen
    points(x=jitter(values$Xmx, 1), y=y.values)
    
    title(main=titel, cex.main=1.3)
}


#********************
# Bildchen speichern
#dev.new(width=9, height=5)
png(file=paste("GNUtime_Java_Xmx_", TOOL, "_", SAMPLE, ".png", sep=""),
    width=9, height=3.5, units="in", res=160)

par(mfrow = c(1,3))
par(mar=c(4,4,3,1),
    oma=c(0,0,2,0)
   )


# cpu time wall
Diagramm(x=mean.wall$Xmx,
	 y=mean.wall$wall,
	 y.values=values$wall,
	 text.abszisse="Java Heap Space (-Xmx) Gb",
	 text.ordinate="CPU time (min)",
	 titel="Consumed CPU time wall"
	)

# cpu time user
Diagramm(x=mean.user$Xmx,
	 y=mean.user$user,
	 y.values=values$user,
	 text.abszisse="Java Heap Space (-Xmx) Gb",
	 text.ordinate="CPU time (min)",
	 titel="Consumed CPU time user"
	)

# max RSS
Diagramm(x=mean.RSS$Xmx,
	 y=mean.RSS$RSS,
	 y.values=values$RSS,
	 text.abszisse="Java Heap Space (-Xmx) Gb",
	 text.ordinate="RSS (Gb)",
	 titel="Maximum resident set size"
	)

# Linie mit Steigung 1 einzeichnen
# 1 Gb Xmx = 1 Gb RSS
# zeigt wann weniger als die max. mögliche RAM-Menge genutzt wird
abline(0,1, col="grey")

# äußerer Haupttitel der Graphik
title(main=paste("Java Heap Space and GATK ", TOOL, sep=""), outer=TRUE, cex.main=1.4)

# Bildchen Ende
dev.off()

#**************************************************************
# Bonus: Spearman'sche Korrelationskoeffizient der Mittelwerte
#**************************************************************

spearRSS  = cor(x=mean.RSS$Xmx, y=mean.RSS$RSS, method="spear")
spearUser = cor(x=mean.user$Xmx, y=mean.user$user, method="spear")
spearWall = cor(x=mean.wall$Xmx, y=mean.wall$wall, method="spear")
spearR.tMem = cor(x=mean.R.tMem$Xmx, y=mean.R.tMem$R.tMem, method="spear")

print("Spearman'scher Korrelationskoeffizient der Mittelwerte:")
print(paste("  Wall:  ", round(spearWall, digits=3)) )
print(paste("  User:  ", round(spearUser, digits=3)) )
print(paste("  RSS:   ", round(spearRSS, digits=3)) )
