==========
Change Log
==========

1.2.0
=====

- Improved scheduling of bwa memory usage (especially useful for Slurm).
- Documentation for bwa memory scheduling added.
- Improved documentation for memory scheduling via config.yaml.
- Two new alternative target rules added to the workflow.
- Documentation provided for these new target rules.
- Minor corrections to the documentation, such as typos and wording.

1.1.0
=====

- Improved scheduling of memory usage (especially useful for Slurm usage).
- Documentation for Slurm usage added.
- Java memory overhead enabled for better resource scheduling (affects Snakefile, config.yaml and documentation).
- Some refactoring of the Snakefile (mainly formatting).
- Change log added to documentation.
- Citation of final publication added.

1.0.1
=====

Various improvements were added to the documentation. No changes to the workflow.

- New section about error identification added.
- Better explanation of snakemake options -j and --cores.
- bioRxiv link added.
- Small fixes, like typos.

1.0
===

- Initial release of OVarFlow, including the "normal" variant calling workflow and the optional BQSR workflow.
