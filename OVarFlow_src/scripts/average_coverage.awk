# calculates the average coverage of a bam file by using
# samtools depth -a as input values
# SAQ (Summe der Abeichungsquadrate) = SDD (sum of squared deviations)
#
# SDD = sumsq - NR * mean * mean
# var = 1 / (NR -1) * SDD
# -1 can be neglected with the very high numbers that are given
# the equation of var is therefore simplified to
# var = sumsq/NR - mean*mean
{
	sum += $3
	sumsq += $3 * $3
}
END{
	mean = sum / NR
	var = sumsq/NR - mean*mean
	stddev = sqrt(var)
	print "average coverage: " mean
	print "standard deviation: " stddev
}
