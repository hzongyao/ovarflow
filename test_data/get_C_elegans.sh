#!/usr/bin/env bash

mkdir Fastq
mkdir Reference

# C. elegans Genome:
# https://www.ncbi.nlm.nih.gov/genome/?term=Caenorhabditis+elegans
# Reference genome: WBcel235
# https://www.ncbi.nlm.nih.gov/assembly/GCA_000002985.3
# Genome size: 100,286,401

# genome and annotation
wget --background -P Reference https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/002/985/GCF_000002985.6_WBcel235/GCF_000002985.6_WBcel235_genomic.fna.gz
wget --background -P Reference https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/002/985/GCF_000002985.6_WBcel235/GCF_000002985.6_WBcel235_genomic.gff.gz

# Sample data: study PRJNA490381
# https://www.ebi.ac.uk/ena/data/view/PRJNA490381
# Illumina HiSeq 3000, paired end

# Reads 1
# Run: SRR7872876
# Base Count: 3,228,228,698
# samtools depth & awk: 26 x coverage
wget --background -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR787/006/SRR7872876/SRR7872876_1.fastq.gz
wget -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR787/006/SRR7872876/SRR7872876_2.fastq.gz

# Reads 2
# Run: SRR7872879
# Base Count: 3,452,739,122
# samtools depth & awk: 28 x coverage
wget --background -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR787/009/SRR7872879/SRR7872879_1.fastq.gz
wget -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR787/009/SRR7872879/SRR7872879_2.fastq.gz

# Reads 3
# Run: SRR7872880
# Base Count: 3,191,992,624
# samtools depth & awk: 25 x coverage
wget --background -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR787/000/SRR7872880/SRR7872880_1.fastq.gz
wget -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR787/000/SRR7872880/SRR7872880_2.fastq.gz

# Reads 4
# Run: SRR7872886
# Base Count: 3,463,738,264
# samtools depth & awk: 28 x coverage
wget --background -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR787/006/SRR7872886/SRR7872886_1.fastq.gz
wget -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR787/006/SRR7872886/SRR7872886_2.fastq.gz
