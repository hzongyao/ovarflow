Test data sets for OVarFlow
==============================

Several scripts are available to obtain different test data sets for OVarFlow. All of these data sets contain the sequence files of four individuals. In a typical analysis the number of individuals would be much larger.

Each download script will obtain:

* a reference sequence
* a reference annotation
* four paired end Illumina sequencing files (fastq.gz)

Additionally two directories will be created within the working directory, being `Fastq` and `Reference`. The respective files will be placed into those directories.

In principle three different data sets can be obtained:


### C. elegans

Download script: `get_C_elegans.sh`

*C. elegans* has a moderately sized genome of approx. 100 mio bp. Also the number of variants is considerably low than in chicken. As HaplotypeCaller reassembles the reads at every variant location, the runtime of the HaplotypeCaller is drastically decreased.


### Chicken

Download script: `get_chicken_..._coverage.sh`

For chicken (*Gallus gallus*) three different download scripts are provided. As the file name suggests the average coverage of the paired end fastq files will vary. During the development of OVarFlow different sequencing depths were benchmarked.


### Minimum test data set

Script: `create_mini_data_set.sh`

This is actually not a download script. Instead this script will create a very tiny sample data set from a previously downloaded chicken data set. Therefore execution of on of the chicken download scripts is mandatory first. This script will then map those reads and extract only those reads mapping to four contigs of the chicken genome. Thereby a very tiny genome and its according sequencing data are created. This shrunken data set was used in the development of OVarFlow, as it allowed for a very quick test of the entire Workflow.

This script has to be executed in the same directory where the download script was executed beforehand. A possible combination of commands would therefore look like this:

```
./get_chicken_low_coverage.sh
./create_mini_data_set.sh
```
