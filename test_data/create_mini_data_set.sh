#!/usr/bin/env bash
set -euo pipefail

# due to mapping with bwa mem: 4 data sets, 6 threads each
echo -e "\n\n"
echo "WARNING!"
echo "This workflow wants to run on a 28 core machine!"
echo "Approx. runtime 1 1/4 h."
echo -e "\n\n"

# let's embed some Python3 code as a bash function ;)
function get_fastq_seqs() {
python3 - <<END
#!/usr/bin/env python3

# usage:
# ./get_fastq_seq.py <fastq_reads.fq.gz> <fastq_header_list.txt>
# the script extracts all reads from a compressed fastq file (.fq.gz) that
# are contained within the list of reads (fastq headers)

import gzip
import sys

fastq_reads = '${1}'
fasta_headers = '${2}'

# alle Fasta-Header einlesen und in einem set speichern
# (kann schneller durchsucht werden als eine Liste)
header_set = set()
with open(fasta_headers) as fhs:
    header = "start"
    while header:
        header = fhs.readline().rstrip()
        header_set.add(header)

print("Fasta-Header eingelesen:", len(header_set), file=sys.stderr)

# alle im set vorhanden header aus der Fastq-Datei herausschreiben
def extract_headers(fq_file):
    with gzip.open(fq_file, 'rt') as R1reads:
        line_1 = "start"
        while line_1:
            # fastq-Sequenze vollständig einlesen
            line_1 = R1reads.readline()
            line_2 = R1reads.readline()
            line_3 = R1reads.readline()
            line_4 = R1reads.readline()
    
            # header muss passend zum Set formatiert sein (kein @)
            fq_header = line_1.split(" ")[0][1:]
            
            # falls header im set vorhanden, dann ausgeben
            if fq_header in header_set:
                print(line_1, line_2, line_3, line_4, sep='', end='')

extract_headers(fastq_reads)
END
}

GENOME=./Reference/GCF_000002315.6_GRCg6a_genomic.fna.gz
ANNOTATION=./Reference/GCF_000002315.6_GRCg6a_genomic.gff.gz
READS1=(`ls ./Fastq/SRR*_1.fastq.gz`) # array of all fastq files
FQ1_NAMES=(${READS1[@]##*/}) # chops off the path prefix
# list of all SRR names - no array just a string separated by ' '
SRR_NAMES=${FQ1_NAMES[@]%_1.fastq.gz} # chops off the suffix _1.fastq.gz


SAMPLE_DIR_ref=sample/mini_reference
SAMPLE_DIR_map=sample/mapping
SAMPLE_DIR_faq=sample/fastq


mkdir -p $SAMPLE_DIR_ref
mkdir -p $SAMPLE_DIR_map
mkdir -p $SAMPLE_DIR_faq

# Does an index for the reference genome exit?
if ! [ -f ${GENOME}.fai ]
    then
    echo " Building index for reference genome."
    echo " ===================================="
    gunzip -c $GENOME | bgzip > ${GENOME%gz}bgz
    samtools faidx ${GENOME%gz}bgz
fi

echo " Extract sample genome."
echo " ======================"
samtools faidx ${GENOME%gz}bgz \
    NC_006119.4 \
    NW_020110163.1 \
    NW_020110162.1 \
    NC_028739.2 \
    > ${SAMPLE_DIR_ref}/SampleSeq.fa


echo " Extract sample annotation."
echo " =========================="
#zcat $ANNOTATION | head -6 > ${SAMPLE_DIR_ref}/SampleAnn.gff # head -6 causes pipe to fail
# => script termiantes after head (if set -eo pipefail)
# only if gff file is longer than line 140, no explaination found
# using sed instead
zcat $ANNOTATION | sed -n '1,6p' > ${SAMPLE_DIR_ref}/SampleAnn.gff
for contig in NC_006119.4 NW_020110163.1 NW_020110162.1 NC_028739.2
do
    zgrep -F "${contig}" $ANNOTATION >> ${SAMPLE_DIR_ref}/SampleAnn.gff
done


echo " Create bwa index."
echo " ================="
# create index for bwa mem
bwa index ${SAMPLE_DIR_ref}/SampleSeq.fa

echo " Mapping of reads."
echo " ================="
# save only mapped reads to output (-F 4)
for name in ${SRR_NAMES}
do
    bwa mem -M -t 6 ${SAMPLE_DIR_ref}/SampleSeq.fa \
	Fastq/${name}_1.fastq.gz Fastq/${name}_2.fastq.gz \
	2> ${SAMPLE_DIR_map}/${name}_bwa.log | \
	samtools view -F 4 -b /dev/fd/0 -o ${SAMPLE_DIR_map}/${name}.bam &
done
wait

echo " Get fastq headers."
echo " =================="
# get the uniq fasta headers of mapped reads
for mappings in ${SAMPLE_DIR_map}/*.bam
do
    samtools view -S $mappings | cut -f 1 | uniq > ${mappings%.bam}_headers.txt &
done
wait

echo " Extract sample read set."
echo " ========================"
for name in $SRR_NAMES
do
    echo "Processing sample: $name"
    get_fastq_seqs Fastq/${name}_1.fastq.gz ${SAMPLE_DIR_map}/${name}_headers.txt \
        > ${SAMPLE_DIR_faq}/${name}_R1.fastq &
    get_fastq_seqs Fastq/${name}_2.fastq.gz ${SAMPLE_DIR_map}/${name}_headers.txt \
	> ${SAMPLE_DIR_faq}/${name}_R2.fastq &
done
wait

echo " Compress sample reads."
echo " ======================"
for file in ${SAMPLE_DIR_faq}/*.fastq
do
    gzip ${file} &
done
wait

echo " Final test that R1 and R2 are identically ordered."
echo " =================================================="
# final check that R1 & R2 reads are in identical order
for name in $SRR_NAMES
do
    diff -sq <(zcat ${SAMPLE_DIR_faq}/${name}_R1.fastq | sed -n '1~4p' | cut -d'/' -f1) \
             <(zcat ${SAMPLE_DIR_faq}/${name}_R2.fastq | sed -n '1~4p' | cut -d'/' -f1) &
done
wait
